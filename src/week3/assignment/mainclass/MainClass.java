package week3.assignment.mainclass;

import java.util.Scanner;

import week3.assignment.bookoperations.MagicOfBooks;
import week3.assignment.exception.CustomException;

public class MainClass {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		MagicOfBooks m=new MagicOfBooks();

		while (true) {
			System.out.println("1.Add Book");
			System.out.println("2.Delete entries from Book");
			System.out.println("3.Update a book");
			System.out.println("4.Display all books");
			System.out.println("5.Count Total books");
			System.out.println("6.Show for Autobiography books");
			System.out.println("7.Display Books by features");
			System.out.println("0.Enter 0 To Exit");

			System.out.println("enter your choice:");
			int choice = sc.nextInt();
			if(choice == 0) {
				System.out.println("Application Stopped");
				break;
			}
			switch (choice) {

			case 1:
				System.out.println("Please Enter Number of Books you want to add");
				int n=sc.nextInt();

				for(int i=1;i<=n;i++) {
					try {
						m.addbook();
					} catch (CustomException e) {
						System.out.println(e.getMessage());
					}
				}
				break;

			case 2:
				m.deletebook();
				break;

			case 3:
				try {
					m.updatebook();
				} catch (CustomException e1) {
					System.out.println(e1.getMessage());
				}
				break;

			case 4:
				try {
					m.displayBookInfo();
				} catch (CustomException e) {
					System.out.println(e.getMessage());
				}
				break;

			case 5:
				m.count();
				break;

			case 6:
				try {
					m.autobiography();
				} catch (CustomException e) {
					System.out.println(e.getMessage());
				}
				break;


			case 7:
				System.out.println("Enter your choice:");
				System.out.println("1.Price low to high");
				System.out.println("2.Price high to low");
				System.out.println("3.Best selling");
				int ch = sc.nextInt();

				switch (ch) {

				case 1:
					try {
						m.displayByFeature(1);
					} catch (CustomException e) {
						System.out.println(e.getMessage());
					}
					break;
				case 2:
					try {
						m.displayByFeature(2);
					} catch (CustomException e) {
						System.out.println(e.getMessage());
					}
					break;
				case 3:
					try {
						m.displayByFeature(3);
					} catch (CustomException e) {
						System.out.println(e.getMessage());
					}
					break;
				default :
					System.out.println("Invalid Input");
					break;
				}
				break;

			default:
				System.out.println("entered wrong choice.!");
				break;
			}

		}

	}

}
