package week3.assignment.bookoperations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import week3.assignment.book.Book;
import week3.assignment.exception.CustomException;

public class MagicOfBooks {

	Scanner scanner=new Scanner(System.in);


	HashMap<Integer,Book> HashBookMap=new HashMap<>();
	TreeMap<Double,Book> treemap=new TreeMap<>();

	ArrayList<Book> booklist=new ArrayList<>();

	public  void addbook() throws CustomException{
		int value  =1;
		Book b= new Book();
		System.out.println("Enter a book id");
		Set<Integer> keySet = HashBookMap.keySet();
		int bid = scanner.nextInt();
		if(HashBookMap.isEmpty()) {
			b.setId(bid);
		}else {
			for(Integer i:keySet) {
				if(i == bid) {
					value = 0;
					throw new CustomException("Book ID Already Exist");
				}else {
					value = 1;
				}
			}
		}
		if(value == 1) {
			b.setId(bid);
		}else {
			throw new CustomException("Book ID Already Exists in DB");
		}
		//b.setId(scanner.nextInt());
		System.out.println("enter a book name");
		b.setName(scanner.next());

		System.out.println("enter a book price");
		b.setPrice(scanner.nextDouble());

		System.out.println("enter a book genre");
		b.setGenre(scanner.next());

		System.out.println("enter number of copies sold");
		b.setNoOfCopiesSold(scanner.nextInt());

		System.out.println("enter a book status");
		b.setBookstatus(scanner.next());

		HashBookMap.put(b.getId(), b);
		System.out.println("Book added successfully");

		treemap.put(b.getPrice(), b);
		booklist.add(b);

	}

	public void deletebook() {

		System.out.println("Enter book id you want to delete");
		int bid = scanner.nextInt();
		for(Book bookID : booklist) {
			if(bookID.getId() == bid) {
				HashBookMap.remove(bid);
				System.out.println("Book Deleted Successfully");
			}
		}
	}

	public void updatebook() throws CustomException{

		int value = 1;
		Book b= new Book();
		System.out.println("Enter a book id");
		int bid = scanner.nextInt();
		for(Book bk : booklist) {
			if(bk.getId() == bid) {
				b.setId(bid);
				value = 1;
			}else {
				value = 0;
			}
		}
		if(value == 0) {
			throw new CustomException("Book for given ID is not Found for Updation");
		}
		System.out.println("Enter Your Book Name to be updated");
		b.setName(scanner.next());

		System.out.println("Enter the Price that Your want to Update");
		b.setPrice(scanner.nextDouble());

		System.out.println("Enter the Genre that Your want to Update");
		b.setGenre(scanner.next());

		System.out.println("Enter the number of copies sold for Updation");
		b.setNoOfCopiesSold(scanner.nextInt());

		System.out.println("Enter the updated status");
		b.setBookstatus(scanner.next());

		HashBookMap.replace(b.getId(), b);
		System.out.println("Book details Updated successfully");
	}

	public void displayBookInfo() throws CustomException {

		if (HashBookMap.size() > 0){
			Set<Integer> keySet = HashBookMap.keySet();

			for (Integer key : keySet) {

				System.out.println(key + " : " + HashBookMap.get(key));
			}
		} else {
			throw new CustomException("Book list is Empty");
		}

	}

	public void count() {

		System.out.println("Number of books present in a Library"+HashBookMap.size());

	}

	public void autobiography() throws CustomException {
		String bestSelling = "Autobiography";

		ArrayList<Book> genreBookList = new ArrayList<Book>();

		Iterator<Book> iter=(booklist.iterator());
		while(iter.hasNext()){
			Book b1=(Book)iter.next();
			if(b1.genre.equals(bestSelling)){
				genreBookList.add(b1);
				System.out.println(genreBookList);
			}
		}  
	}

	public void displayByFeature(int ch) throws CustomException {

		if(ch==1){
			if (treemap.size() > 0) {
				Set<Double> keySet = treemap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " : " + treemap.get(key));
				}
			} else {
				throw new CustomException("TreeMap is Empty");
			}
		}
		if(ch==2) {
			Map<Double, Object> treeMapReverseOrder= new TreeMap<>(treemap);
			NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();

			System.out.println("Book Details:");

			if (nmap.size() > 0) {
				Set<Double> keySet = nmap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " : " + nmap.get(key));
				}
			}else{
				throw new CustomException("TreeMap is Empty");
			}

		}

		if(ch==3){
			String bestSelling = "BestSelling";
			ArrayList<Book> genreBookList = new ArrayList<Book>();

			Iterator<Book> iter=booklist.iterator();

			while(iter.hasNext()){
				Book b=(Book)iter.next();
				if(b.bookstatus.equalsIgnoreCase(bestSelling)) {
					genreBookList.add(b);
				}

			}
			if(genreBookList.size() > 0) {
				System.out.println(genreBookList);
			}else {
				throw new CustomException("BestSelling Book List is Empty");
			}

		}
	}

}

