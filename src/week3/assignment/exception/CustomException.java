package week3.assignment.exception;

public class CustomException extends Exception{
	
	public CustomException(String str) {
		super(str);
	}
	
}
